﻿namespace SwanSong.Domain.Model.Settings
{
    public class SendGridSettings
    {
        public string ApiKey { get; set; }
        public string EmailFrom { get; set; }
    }
}
